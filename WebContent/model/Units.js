sap.ui.define([ "sap/ui/base/Object", 
				"sap/ui/model/json/JSONModel",
				"com/qs/qgo4web/model/Unit",
				"com/qs/qgo4web/service/ConfigurationService"], function(Object, JSONModel, Unit, ConfigurationService) {
	"use strict";
	return Object.extend("com.qs.qgo4web.model.Units", {
		constructor : function() {
			var units = [];

			var listBuilderCb = function(element, index, array) {
				var unit = new Unit(element);
				units.push(unit);
			}
			
			//TODO: make async request
			var unitsFromServer = new sap.ui.model.json.JSONModel(),
				config = new ConfigurationService();
			
			unitsFromServer.loadData(config.getBacendUrl() + "rest/units/list", null,false, 'GET');
			//unitsFromServer.loadData("http://localhost:8080/QGo4WebUi/qgowebui/model/mock_units.json", null,false, 'GET');
			
			unitsFromServer.oData.forEach(listBuilderCb);
			this.units = units;
			this.model = new JSONModel();
			this.model.setData(this);
		},

		getUnits : function() {
			return this.units;
		},
		getUnit : function(index) {
			return this.units[index];
		},
		getModel : function() {
			return this.model;
		}
	});
});