sap.ui.define([
  "sap/ui/base/Object",
  "sap/ui/model/json/JSONModel"
], function(Object, JSONModel) {
  "use strict";
  return Object.extend("com.qs.qgo4web.model.RunId", {
    constructor: function(data) {
      if (data) {
    	  this.unit = data.unit;
    	  this.runid = data.runid;
    	  this.crTs = data.crTs;
    	  this.crUser = data.crUser;
    	  this.runidName = data.runidName;
      }
      this.model = new JSONModel();
      this.model.setData(this);
    },
    getUnit: function(){
    	return this.unit ;
    },
    getRunid : function(){
    	return this.runid ;
    },
    getCrTs : function(){
    	return this.crTs ;
    },
    getCrUser: function(){
    	return this.crUser ;
    },
    getRunidName: function(){
    	return this.runidName ;
    }
  });
});
