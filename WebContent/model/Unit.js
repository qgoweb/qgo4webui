sap.ui.define([
  "sap/ui/base/Object",
  "sap/ui/model/json/JSONModel"
], function(Object, JSONModel) {
  "use strict";
  return Object.extend("com.qs.qgo4web.model.Unit", {
    constructor: function(data) {
      if (data) {
    	  this.unitDeliveryType = data.unitDeliveryType;
    	  this.unit             = data.unit;
    	  this.tsScenId         = data.tsScenId;
    	  this.tcId             = data.tcId;
    	  this.mlLtpId          = data.mlLtpId;
    	  this.bcrDChainId      = data.bcrDChainId;
    	  this.bcrUChainId      = data.bcrUChainId;
    	  this.unitName         = data.unitName;
      }
      this.model = new JSONModel();
      this.model.setData(this);
    },
    getUnitDeliveryType: function(){
    	return this.unitDeliveryType ;
    },
    getUnit            : function(){
    	return this.unit ;
    },
    getTsScenId        : function(){
    	return this.tsScenId ;
    },
    getTcId            : function(){
    	return this.tcId ;
    },
    getMlLtpId         : function(){
    	return this.mlLtpId ;
    },
    getBcrDChainId     : function(){
    	return this.bcrDChainId ;
    },
    getBcrUChainId     : function(){
    	return this.bcrUChainId ;
    },
    getUnitName        : function(){
    	return this.unitName ;
    }
  });
});
