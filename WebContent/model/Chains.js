sap.ui.define([ "sap/ui/base/Object", 
				"sap/ui/model/json/JSONModel",
				"com/qs/qgo4web/model/Chain",
				"com/qs/qgo4web/service/ConfigurationService"], function(Object, JSONModel, Chain, ConfigurationService) {
	"use strict";
	return Object.extend("com.qs.qgo4web.model.Chains", {
		constructor : function(unit, runid, processType) {						
			if(unit && runid && processType){
				var chains = [];

				var listBuilderCb = function(element, index, array) {
					var chain = new Chain(element);
					chains.push(chain);
				}
				
				var chainsFromServer = new sap.ui.model.json.JSONModel(),
					url,
					config = new ConfigurationService();
				
				url = config.getBacendUrl() + "rest/chains/list?runid="+ runid + "&unit=" + unit + "&pt=" + processType;
				
				chainsFromServer.loadData(url, null,false, 'GET');
				
				chainsFromServer.oData.forEach(listBuilderCb);
				
				this.chains = new sap.ui.model.json.JSONModel(chains);
				this.model = new JSONModel();
				this.model.setData(this);
			}
		},

		getChains : function() {
			return this.chains;
		},
		getChain : function(index) {
			return this.chains[index];
		},
		getModel : function() {
			return this.model;
		}
	});
});