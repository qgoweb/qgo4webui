sap.ui.define([
  "sap/ui/base/Object",
  "sap/ui/model/json/JSONModel"
], function(Object, JSONModel) {
  "use strict";
  return Object.extend("com.qs.qgo4web.model.Chain", {
    constructor: function(data) {
      if (data) {
    	  this.unit        = data.unit
    	  this.runid       = data.runid       
    	  this.chainId     = data.chainId     
    	  this.itemMainNr  = data.itemMainNr  
    	  this.itemSubNr   = data.itemSubNr   
    	  this.state       = data.state       
    	  this.status      = data.status      
    	  this.statusIcon  = data.statusIcon  
    	  this.activityTxt = data.activityTxt 
    	  this.tsStartDisp = data.tsStartDisp 
    	  this.tsEndDisp   = data.tsEndDisp   
    	  this.objType     = data.objType     
    	  this.objName     = data.objName     
    	  this.tsCreateDb  = data.tsCreateDb  
      }
      this.model = new JSONModel();
      this.model.setData(this);
    },
    toPlainObject: function(){
    	return {
    		"unit": this.getUnit(),
    		"runid": this.getRunid(),
    		"chainId": this.getChainId(),
    		"itemMainNr": this.getItemMainNr(),
    		"itemSubNr": this.getItemSubNr(),
    		"state": this.getState(),
    		"status": this.getStatus(),
    		"statusIcon": this.getStatusIcon(),
    		"activityTxt": this.getActivityTxt(),
    		"tsStartDisp": this.getTsStartDisp(),
    		"tsEndDisp": this.getTsEndDisp(),
    		"objType": this.getObjType(),
    		"objName": this.getObjName(),
    		"tsCreateDb": this.getTsCreateDb()
    	};
    },
    toString: function(){
    	return JSON.stringify(this.toPlainObject());
    },
    getUnit: function(){
    	return this.unit ;
    },
    getRunid : function(){
    	return this.runid ;
    },
    getChainId: function(){
    	return this.chainId;
    },
    getItemMainNr: function(){
    	return this.itemMainNr;
    },
    getItemSubNr: function(){
    	return this.itemSubNr;
    },
    getState: function (){
    	return this.state;
    },
    getStatus: function(){
    	return this.status;
    },
    getStatusIcon: function(){
    	return this.statusIcon;
    }, 
    getActivityTxt: function(){
    	return this.activityTxt;
    },
    getTsStartDisp: function(){
    	return this.tsStartDisp;
    }, 
    getTsEndDisp: function(){
    	return this.tsEndDisp;
    },
    getObjType: function(){
    	return this.objType;
    },
    getObjName: function(){
    	return this.objName;
    },
    getTsCreateDb: function(){
    	return this.tsCreateDb;
    }
  });
});
