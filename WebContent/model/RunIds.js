sap.ui.define([ "sap/ui/base/Object", 
				"sap/ui/model/json/JSONModel",
				"com/qs/qgo4web/model/RunId",
				"com/qs/qgo4web/service/ConfigurationService"], function(Object, JSONModel, RunId, ConfigurationService) {
	"use strict";
	return Object.extend("com.qs.qgo4web.model.RunIds", {
		constructor : function(unit) {						
			if(unit){
				var runIds = [];

				var listBuilderCb = function(element, index, array) {
					var runId = new RunId(element);
					runIds.push(runId);
				}
				
				var runIdsFromServer = new sap.ui.model.json.JSONModel(),
					config = new ConfigurationService();
				
				runIdsFromServer.loadData(config.getBacendUrl() + "rest/runids/list?unit=" + unit, null,false, 'GET');
				runIdsFromServer.oData.forEach(listBuilderCb);
				
				this.runIds = runIds;
				this.model = new JSONModel();
				this.model.setData(this);
			}
		},

		getRunIds : function() {
			return this.runIds;
		},
		getRunId : function(index) {
			return this.runIds[index];
		},
		getModel : function() {
			return this.model;
		}
	});
});