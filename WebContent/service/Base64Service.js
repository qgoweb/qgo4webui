sap.ui.define([ "sap/ui/base/Object" ], function(Object) {
	"use strict";
	return Object.extend("com.qs.qgo4web.service.Base64Service", {
		/*https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding*/
		
		b64EncodeUnicode: function(str) {
		    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
		        return String.fromCharCode('0x' + p1);
		    }));
		},
		b64DecodeUnicode: function(str) {
		    return decodeURIComponent(atob(str).split('').map(function(c) {
		        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
		    }).join(''));
		}
	});
});
