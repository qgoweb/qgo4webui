sap.ui.define([ "sap/ui/base/Object" ], function(Object) {
	"use strict";
	return Object.extend("com.qs.qgo4web.service.SelectionBuilderService", {

		build : function(select, list, keyAccessor, textAccessor, displayKeyAndText=false) {
			var newItem = {};
			for (var i = 0; i < list.length; i++) {
				if(displayKeyAndText){
					newItem = new sap.ui.core.Item({
						key : list[i][keyAccessor](),
						text : list[i][keyAccessor]() + " (" + list[i][textAccessor]() + ")"
					});
				}else{
					newItem = new sap.ui.core.Item({
						key : list[i][keyAccessor](),
						text : list[i][textAccessor]()
					});
				}
				
				select.addItem(newItem);
			}
		}
	});
});
