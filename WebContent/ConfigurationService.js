sap.ui.define([ "sap/ui/base/Object"], function(Object) {
	"use strict";
	return Object.extend("com.qs.qgo4web.service.ConfigurationService", {
		
		_configObject : {
			configurationName : "<CONFIGURATIONNAME>",
			backEndUrl : "<BACKENDURL>"
		},
		
		constructor : function() {
		},
		
		getBacendUrl: function(){
			return this._configObject["backEndUrl"];
		},
		getConfigurationName: function(){
			return this._configObject["configurationName"];
		}
	});
});