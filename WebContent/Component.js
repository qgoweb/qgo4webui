sap.ui.define([ "sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel" ],
		function(UIComponent, JSONModel) {
			"use strict";
			return UIComponent.extend("com.qs.qgo4web.Component", {
				metadata : {
					manifest: "json"
				},
				
				createContent : function() {
					this.getRouter().initialize();
					var oView = sap.ui.view({
			            id: "idViewRoot",
			            viewName: "com.qs.qgo4web.view.Root",
			            type: sap.ui.core.mvc.ViewType.XML,
			            viewData: { component : this }
			        });
					
					 window.qgo.appView = oView;
					 
					// done
			        return oView;
				}

			});
		});