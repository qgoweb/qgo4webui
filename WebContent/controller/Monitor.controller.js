sap.ui.define([ 
        "com/qs/qgo4web/controller/BaseController",
		"sap/ui/core/mvc/Controller",
		"sap/ui/core/routing/History",
		"sap/m/Dialog",
		"sap/ui/model/json/JSONModel", 
		"sap/ui/model/Filter", 
		"sap/ui/model/FilterOperator",
		"sap/ui/core/format/DateFormat",
		"sap/ui/model/resource/ResourceModel",
		"com/qs/qgo4web/model/Units",
		"com/qs/qgo4web/model/RunIds",
		"com/qs/qgo4web/model/Chains",
		"com/qs/qgo4web/service/SelectionBuilderService",
		"com/qs/qgo4web/model/Chain",
		"com/qs/qgo4web/service/Base64Service"
		], function(BaseController, JSONModel,Dialog, Filter, FilterOperator, DateFormat, ResourceModel, Units, RunIds, Chains, SelectionBuilderService, Chain, Base64Service) {
	"use strict";

	return BaseController.extend("com.qs.qgo4web.controller.Monitor", {
		
		unit:"",
		runId:"",
		processType:"UP",
		
		onInit : function() {
			
			var i18nModel = new sap.ui.model.resource.ResourceModel({bundleName: "com.qs.qgo4web.i18n.i18n"}),
				unitSelect = this.getView().byId("unit"),
				runIdSelect = this.getView().byId("runId");
				
			this.setModel(i18nModel, "i18n");
			var that = this;
			
			this.loadUnits().then(function(unitsModel){
				var selectionBuilderService = new com.qs.qgo4web.service.SelectionBuilderService(),
					runIdsModel = new com.qs.qgo4web.model.RunIds(unitsModel.getUnit(0).getUnit());
				
				selectionBuilderService.build(unitSelect, unitsModel.getUnits(), "getUnit", "getUnitName", true);				
				selectionBuilderService.build(runIdSelect, runIdsModel.getRunIds(), "getRunid", "getRunid");
				that.unit = unitsModel.getUnit(0).getUnit();
				that.runId = runIdsModel.getRunId(0).getRunid();
				
			}, function(error) {
				  console.error("Failed!", error);
			});
			
		},
		
		loadUnits: function(){
			return new Promise(function(resolve, reject){
				sap.ui.core.BusyIndicator.show(0);
				var unitsModel = new com.qs.qgo4web.model.Units();
				sap.ui.core.BusyIndicator.hide();
				if(unitsModel){
					resolve(unitsModel);
				}
				else{
					reject(Error("Failed to load units"))
				}
			});			
		},
		
		onUnitChange : function() {
			var unit = this.getView().byId("unit").getSelectedItem().getKey(),
				runIdSelect = this.getView().byId("runId"),
				runIdsModel  = [],
				selectionBuilderService = new com.qs.qgo4web.service.SelectionBuilderService();
			
			this.unit = unit;
			sap.ui.core.BusyIndicator.show(0);
			
			//time out is to prevent ui freezing before BusyIndicator starts
			setTimeout(function() {
		        return function() { 
		        	runIdsModel  = new com.qs.qgo4web.model.RunIds(unit);
		        	runIdSelect.destroyItems();
					selectionBuilderService.build(runIdSelect, runIdsModel.getRunIds(), "getRunid", "getRunid");
		        	sap.ui.core.BusyIndicator.hide();
		        }
		    }(), 500);
		},
		
		onRunIdChange: function(){
			this.runId = this.getView().byId("runId").getSelectedItem().getKey();
		},
		
		onProcessTypeChange: function(){
			this.processType = this.getView().byId("processType").getSelectedItem().getKey();
		},
		
		search: function (evt) {
			sap.ui.core.BusyIndicator.show(0);
			var that = this;
			
			//time out is to prevent ui freezing before BusyIndicator starts
			setTimeout(function() {
		        return function() { 
		        	var chains = new com.qs.qgo4web.model.Chains(that.unit, that.runId, that.processType);
					that.setModel(chains.getChains(), "chains");
		        	sap.ui.core.BusyIndicator.hide();
		        }
		    }(), 500);
		},
		
		reset: function (evt) {
			this.setModel(new sap.ui.model.json.JSONModel(), "chains");
			this.setModel(new sap.ui.model.json.JSONModel(), "runids");
		},
		
		openDetails: function (evt) {
			var oRouter = this.getOwnerComponent().getRouter();
			var chain = new com.qs.qgo4web.model.Chain(evt.getSource().getBindingContext("chains").getObject());
			var base64Service = new com.qs.qgo4web.service.Base64Service();
			
			oRouter.navTo("ChainDetail", {
				chainsPath: base64Service.b64EncodeUnicode(chain.toString())
			});
			 
		},
		
		getIcon : function (state) {
			if(state == 3) {
				return "sap-icon://accept";
			} else if (state == 2) {
				return "sap-icon://physical-activity";
			}
		    return "sap-icon://course-book";
		},
		
		getIconColor : function (state) {
			if(state == 3) {
				return "Positive";
			} else if (state == 2) {
				return "Neutral";
			}
		    return "Default";
		}
		
	});
});