sap.ui.define([ 
	"com/qs/qgo4web/controller/BaseController",
    "sap/ui/core/routing/History",
    "sap/ui/model/json/JSONModel"
	], function(BaseController, History, JSONModel) {
	"use strict";

	return BaseController.extend("com.qs.qgo4web.controller.DashBoard", {

		onInit : function() {
		},
	});
});