sap.ui.define([ 
	"com/qs/qgo4web/controller/BaseController",
    "sap/ui/core/routing/History",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "com/qs/qgo4web/service/Base64Service"
	], function(BaseController, History, JSONModel, ResourceModel, Base64Service) {
	"use strict";

	return BaseController.extend("com.qs.qgo4web.controller.ChainDetail", {

		onInit : function(evt) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this),
				i18nModel = new sap.ui.model.resource.ResourceModel({bundleName: "com.qs.qgo4web.i18n.i18n"});
			
			this.setModel(i18nModel, "i18n");
			
			oRouter.getRoute("ChainDetail").attachPatternMatched(
					this._onObjectMatched, this);
		},
		
		_onObjectMatched : function(oEvent) {
			var base64Service = new com.qs.qgo4web.service.Base64Service();
			
			var chainModel = new sap.ui.model.json.JSONModel(JSON.parse(base64Service.b64DecodeUnicode(oEvent.getParameter("arguments").chainsPath)));
			this.setModel(chainModel, "chain");
		},

		onNavBack : function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = this.getOwnerComponent().getRouter();
				oRouter.navTo("monitor");
			}
		}

	});
});