sap.ui.define([ "sap/ui/core/mvc/Controller", "sap/m/GroupHeaderListItem",
		"jquery.sap.global", "sap/ui/model/json/JSONModel" ], function(
		BaseController, jQuery, GroupHeaderListItem, JSONModel) {
	"use strict";

	return BaseController.extend("com.qs.qgo4web.controller.Navigation", {

		onInit : function(evt) {
			
			var model = new sap.ui.model.json.JSONModel(),
				view = this.getView(),
				oPage = view.byId("pgNavigation");
			
			oPage.setNavButtonTooltip("Menü einklappen");
			
			model.loadData("model/menu.json", null, false);
			view.setModel(model);
			var list = view.byId("nav-list");
			list.bindItems({
				path : "/menu",
				template : new sap.m.StandardListItem({
					title : "{menuTitle}",
					type:"Navigation",
					description : "{menuDescription}",
					press: this.navigate,
					icon : "{icon}"
				})
			});
		},

		navigate : function(oEvent) {
			console.log("navigating to "
					+ oEvent.oSource.getBindingContext().getProperty(
							"view"));

			var oRouter = window.qgo.ownerComponent.getRouter();
			
		    oRouter.navTo(oEvent.oSource.getBindingContext().getProperty("view"));
		},
		hideMenu: function(evt){
			var masterPage = window.qgo.appView.byId("idRoot");
			masterPage.setMode(sap.m.SplitAppMode.HideMode);
		}
	});
});