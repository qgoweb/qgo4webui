sap.ui.define([ 
        "com/qs/qgo4web/controller/BaseController",
		"sap/ui/core/mvc/Controller"
		], function(BaseController) {
	"use strict";

	return BaseController.extend("com.qs.qgo4web.controller.Root", {
		
		onInit: function(oEvent) {

	        this.oRoot = this.getView().byId("idRoot");
	        // Have child views use this controller for navigation
	        var that = this;
	        this.oRoot.getMasterPages().forEach(function(oPage) {
	            oPage.getController().navigation = that;
	        });
	        
	        window.qgo.ownerComponent = this.getOwnerComponent();

	    },

	    navTo: function(sPageId, oContext) {
	        this.oRoot.to(sPageId);
	        if (oContext) {
	            this.oRoot.getPage(sPageId).setBindingContext(oContext);
	        }
	    },

	    navBack: function() {
	        this.oRoot.back();
	    }
	    
	});

});